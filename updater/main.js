/**
 * This template is a production ready boilerplate for developing with `CheerioCrawler`.
 * Use this to bootstrap your projects using the most up-to-date code.
 * If you're looking for examples or want to learn more, see README.
 */

const Apify = require("apify");

const {
    utils: { log },
} = Apify;

Apify.main(async () => {
    const { startUrls } = await Apify.getInput();
    log.setLevel(log.LEVELS.DEBUG);
    const requestList = await Apify.openRequestList("start-urls", startUrls);

    const handlePageFunction = async ({ response, request, $ }) => {
        const { Manufacturer } = request.userData;

        if (response.status === 404) {
            log.debug(`Handled item ${request.url}`);
            return {
                Handled: true,
                Message: `404 error, Possible OutOfStock`,
                Url: request.url,
            };
        }

        const price = parseFloat(
            $('meta[property="product:price:amount"]').attr("content")
        );
        const title = $("h1.page-title").text().trim();
        const productId = $('div[class="price-box price-final_price"]').attr(
            "data-product-id"
        );
        const stock =
            $("button.action.tocart").text().indexOf("Agregar") > -1
                ? "InStock"
                : "OutOfStock";

        const product = {
            ProductUrl: request.url,
            ProductName: title,
            ProductId: productId,
            Price: price,
            Stock: stock,
            Manufacturer,
        };

        log.debug(`Saving item ${product.ProductName}`);
        await Apify.pushData(product);
    };

    const crawler = new Apify.CheerioCrawler({
        requestList,
        // Be nice to the websites.
        // Remove to unleash full power.
        maxConcurrency: 50,
        handlePageFunction,
    });

    log.info("Starting the crawl.");
    await crawler.run();
    log.info("Crawl finished.");
});
